## Restaurant Website Project ##

Leo's restaurant is an imaginary Restaurant Website which is built for university's assignment. The main goal was to create responsive website from scratch and implement LocalStorage functionality on the reservation page so user don’t have to fill form data again and again.  All the designs for the website are made using Photoshop.

[Link to Website](http://restaurant.npdesign.co.nz/)
