$('document').ready(function(){
	
	// if the user hs support for local storage
	if( Modernizr.localstorage ) {

		//find the advanced-form-function element and insert button
		//$('#advanced-form-function').append('<button id="save">Save</button>');

		// listen to save button for click
		$('.submit').click(saveForm);

		

		// if the user has information stored in the browser
		// display that information in the form
		if ( localStorage.getItem('firstname') ||  localStorage.getItem('email') || localStorage.getItem( 'date')) {
			// prompt the user to populate thair form
			var response = confirm('Do you want to use save form data');
			// if user said yes. than populate form saved data
			if ( response){
				populateForm();
			}
			else{
				// user dose not want daata than delet it
				response = confirm('do you want to delet your data?')

				if (response){
					//delete everything in localstorage
					localStorage.clear();
				}
			}

		}
		
	}

});

function saveForm(){

	// find the user input element

	if ( ( $('#date').val() ) != ''){
		// save the date into the browser using local storage
		localStorage.setItem ( 'date', $('#date').val()  );
	}

	if (  $('#month').val()  != ''){
		// save the month into the browser using local storage
		localStorage.setItem ( 'month', $('#month').val()  );
	}

	if (  $('#year').val()  != ''){
		// save the year into the browser using local storage
		localStorage.setItem ( 'year', $('#year').val()  );
	}

	if (  $('#hours').val()  != ''){
		// save the hour into the browser using local storage
		localStorage.setItem ( 'hours', $('#hours').val()  );
	}

	if (  $('#minits').val()  != ''){
		// save the minits into the browser using local storage
		localStorage.setItem ( 'minits', $('#minits').val()  );
	}

	if (  $('#ampm').val()  != ''){
		// save the ampm into the browser using local storage
		localStorage.setItem ( 'ampm', $('#ampm').val()  );
	}

	if (  $('#guest').val()  != ''){
		// save the gueat into the browser using local storage
		localStorage.setItem ( 'guest', $('#guest').val()  );
	}

	if (  $('#booking-type').val()  != ''){
		// save the booking-type into the browser using local storage
		localStorage.setItem ( 'booking-type', $('#booking-type').val()  );
	}

	if ( $.trim( $('#firstname').val() ) != ''){
		// save the name into the browser using local storage
		localStorage.setItem ( 'firstname', $('#firstname').val()  );
	}
	//alert( localStorage.getItem('firstname') );

	if ( $.trim( $('#email').val() ) != ''){
		// save the email into the browser using local storage
		localStorage.setItem ( 'email', $('#email').val()  );
	}
	//alert( localStorage.getItem('email') );
	if( $('#txt-area').val()  != '' ) {
		localStorage.setItem ( 'txt-area', $('#txt-area').val()  );
	} else {
		localStorage.setItem ( 'txt-area', '' );
	}



	//displaying data on page when conforming booking
	$('#dt').html("<br>You have Booked table on " + localStorage.getItem('date') + " " + localStorage.getItem('month') +", " + localStorage.getItem('year')
					+ " at " + localStorage.getItem('hours') + ":" + localStorage.getItem('minits') + localStorage.getItem('ampm') );

	$('#booking_type').html("<br>Booking Type: " + localStorage.getItem('booking-type') + " for " + localStorage.getItem('guest') + " Guests" );

	$('#name').html("<br>Name: " + localStorage.getItem('firstname') );
	$('#eml').html("<br>Email: " + localStorage.getItem('email') );

	if( localStorage.getItem('txt-area') != '' ) {
		$('#txtar').html("<br>Any Special request: " + localStorage.getItem('txt-area') );
	} else {
		$('#txtar').html('');
	}
	
}

function populateForm(){


	$('#date').val( localStorage.getItem( 'date') );
	$('#month').val( localStorage.getItem( 'month') );
	$('#year').val( localStorage.getItem( 'year') );

	$('#hours').val( localStorage.getItem( 'hours') );
	$('#minits').val( localStorage.getItem( 'minits') );
	$('#ampm').val( localStorage.getItem( 'ampm') );
	$('#guest').val( localStorage.getItem( 'guest') );

	$('#booking-type').val( localStorage.getItem( 'booking-type') );

	$('#txt-area').val( localStorage.getItem( 'txt-area') );

	//select  the full name input  and insert the old full name
	$('#firstname').val( localStorage.getItem( 'firstname') );
	//select  the email  input  and insert the email
	$('#email').val( localStorage.getItem( 'email') );

}

console.log(localStorage);


var overlay = document.getElementById('overlay');

function openModal(){
	overlay.classList.remove("is-hidden");
}

function closeModalByIcon(){
	overlay.classList.add("is-hidden");
}


function closeModal(){
	overlay.classList.add("is-hidden");
	
	$('#firstname, #email, #txt-area').val('');
	
	$("#date").val("Day");
	$('#month').val('Month');
	$('#year').val('Year');

	$('#hours').val('Hours');
	$('#minits').val('Minutes');
	$('#ampm').val('am/pm');

	$('#guest').val('Number of Guests');
	$('#booking-type').val('choose one');
	
	localStorage.clear();
}
// $('.register').on('submit', validate);


function validate( ){
	
	var formValid = true;

	// chacking if email is valid or not
	var sEmail = $('#email').val();
		
		// Checking Empty Fields
		if ($.trim(sEmail).length == 0) {
			// alert('All fields are mandatory');
			$('#email').next().html('All fields are mandatory');
			$('#email').css({"border-color": "#D72823", 
	             "border-width":"1px", 
	             "border-style":"solid"});
			// e.preventDefault();
			formValid = false;
		}
		
		else if (validateEmail(sEmail)) {
			// alert('Nice!! your Email is valid, now you can continue..');
			$('#email').next().html('');
			$('#email').css({"border-color": "#aaa", 
		     "border-width":"1px", 
		     "border-style":"solid"});
		}
		
		else {
			// alert('Invalid Email Address');
			$('#email').css({"border-color": "#D72823", 
	             "border-width":"1px", 
	             "border-style":"solid"});
			$('#email').next().html('Invalid Email Address');
			// e.preventDefault();
			formValid = false;
		}

		// chacking if user has select date in drop down menu

	var  date = $('#date option:selected');

		if (date.length == 0 || $(date).val() == "" || $(date).val() == "Day") {
			$('#date').css({"border-color": "#D72823", 
		     "border-width":"1px", 
		     "border-style":"solid"});
		    formValid = false;
		}

		else{
			$('#date').css({"border-color": "#aaa", 
		     "border-width":"1px", 
		     "border-style":"solid"});
		}


		// chacking if user has select month in drop down menu

	var  month = $('#month option:selected');

		if (month.length == 0 || $(month).val() == "" || $(month).val() == "Month") {
			$('#month').css({"border-color": "#D72823", 
		     "border-width":"1px", 
		     "border-style":"solid"});
		    formValid = false;
		}

		else{
			$('#month').css({"border-color": "#aaa", 
		     "border-width":"1px", 
		     "border-style":"solid"});
		}


		// chacking if user has select year in drop down menu

	var  year = $('#year option:selected');

		if (year.length == 0 || $(year).val() == "" || $(year).val() == "Year") {
			$('#year').css({"border-color": "#D72823", 
		     "border-width":"1px", 
		     "border-style":"solid"});
		    formValid = false;
		}

		else{
			$('#year').css({"border-color": "#aaa", 
		     "border-width":"1px", 
		     "border-style":"solid"});
		}


		// chacking if user has select hours in drop down menu

	var  hours = $('#hours option:selected');

		if (hours.length == 0 || $(hours).val() == "" || $(hours).val() == "Hours") {
			$('#hours').css({"border-color": "#D72823", 
		     "border-width":"1px", 
		     "border-style":"solid"});
		    formValid = false;
		}

		else{
			$('#hours').css({"border-color": "#aaa", 
		     "border-width":"1px", 
		     "border-style":"solid"});
		}


		// chacking if user has select Minutes in drop down menu

	var  Minutes = $('#minits option:selected');

		if (Minutes.length == 0 || $(Minutes).val() == "" || $(Minutes).val() == "Minutes") {
			$('#minits').css({"border-color": "#D72823", 
		     "border-width":"1px", 
		     "border-style":"solid"});
		    formValid = false;
		}

		else{
			$('#minits').css({"border-color": "#aaa", 
		     "border-width":"1px", 
		     "border-style":"solid"});
		}


		// chacking if user has select an/pm in drop down menu

	var  ampm = $('#ampm option:selected');

		if (ampm.length == 0 || $(ampm).val() == "" || $(ampm).val() == "am/pm") {
			$('#ampm').css({"border-color": "#D72823", 
		     "border-width":"1px", 
		     "border-style":"solid"});
		    formValid = false;
		}

		else{
			$('#ampm').css({"border-color": "#aaa", 
		     "border-width":"1px", 
		     "border-style":"solid"});
		}


		// chacking if user has select guest in drop down menu

	var  guest = $('#guest option:selected');

		if (guest.length == 0 || $(guest).val() == "" || $(guest).val() == "Number of Guests") {
			$('#guest').css({"border-color": "#D72823", 
		     "border-width":"1px", 
		     "border-style":"solid"});
		    formValid = false;
		}

		else{
			$('#guest').css({"border-color": "#aaa", 
		     "border-width":"1px", 
		     "border-style":"solid"});
		}


		// chacking if user has select booking-type in drop down menu

	var  bookingType = $('#booking-type option:selected');

		if (bookingType.length == 0 || $(bookingType).val() == "" || $(bookingType).val() == "choose one") {
			$('#booking-type').css({"border-color": "#D72823", 
		     "border-width":"1px", 
		     "border-style":"solid"});
		    formValid = false;
		}

		else{
			$('#booking-type').css({"border-color": "#aaa", 
		     "border-width":"1px", 
		     "border-style":"solid"});
		}

		
		// checking if use put name in the field

	var firstname = $('#firstname').val();

		 if ( firstname.length == 0) {
			$('#firstname').css({"border-color": "#D72823", 
             "border-width":"1px", 
             "border-style":"solid"});
		// alert("give ur name"); 
		   // e.preventDefault();
		formValid = false;
		}

		else{
			$('#firstname').css({"border-color": "#aaa", 
		     "border-width":"1px", 
		     "border-style":"solid"});
        }


        //chacking if all fields has been fill or not
        //if all condition match than open pop up window
        //otherwise show erros on page

		if (formValid == true){
			openModal();
		}

}


	// Function that validates email address through a regular expression.
	function validateEmail(sEmail) {
	var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
		if (filter.test(sEmail)) {
			return true;
		}
		else {
			return false;
		}
	}